require "json"

# Sets up a scene with a camera and a 2D HUD and loads objets from a .json file.
# Create a child class and .json file to create a game scene.
class Scene
	attr_accessor :scene, :camera, :hud_scene, :hud_camera
	
	# Loads ressources from the speficied .json file.
	# path should be the same as the class filename: my_scene.rb -> my_scene.json
	def initialize(name)
		@c = Mittsu::Clock.new
		
		@hud_scene = Mittsu::Scene.new
		@hud_camera = Mittsu::OrthographicCamera.new(800, 0, 600, 0, -100)
		
		json = JSON.parse(File.read("scenes/" + name + ".json"))
		
		# Load overlays
		@scene_overlays = Hash.new
		json["overlays"].each do |e|
			@scene_overlays[e[0]] = DisplayUtil.make_overlay_mesh("2d/" + e[1]["file"], e[1]["size"][0].to_f, e[1]["size"][1].to_f)
			
			if e[1]["pos"]
				@scene_overlays[e[0]].position.x = e[1]["pos"][0]
				@scene_overlays[e[0]].position.y = e[1]["pos"][1] 
			end
			
			@hud_scene.add(@scene_overlays[e[0]])
		end if json["overlays"]
		
		@scene = Mittsu::Scene.new
		@camera = Mittsu::PerspectiveCamera.new(80.0, ASPECT, 1.0, 1500.0)
		@camera.position.x = json["camera_pos"][0]
		@camera.position.y = json["camera_pos"][1]
		@camera.position.z = json["camera_pos"][2]
		
		# Load skybox
		if json["skybox"] and not DEBUG_FASTLOAD
			texture = Mittsu::ImageUtils.load_texture_cube(
			  [ 'rt', 'lf', 'up', 'dn', 'bk', 'ft' ].map { |path| File.join File.dirname(__FILE__), 'skybox/' + json["skybox"]["file"], "#{path}." + json["skybox"]["ext"]})
					
			shader = Mittsu::ShaderLib[:cube]
			shader.uniforms['tCube'].value = texture
			skybox_material = Mittsu::ShaderMaterial.new({
			  fragment_shader: shader.fragment_shader,
			  vertex_shader: shader.vertex_shader,
			  uniforms: shader.uniforms,
			  depth_write: false,
			  side: Mittsu::BackSide
			})
			
			skybox = Mittsu::Mesh.new(Mittsu::BoxGeometry.new(1000, 1000, 1000), skybox_material)
			scene.add(skybox)
		end
		
		if json["ambient_light"]
			@scene_amb = Mittsu::AmbientLight.new(json["ambient_light"].to_i(16))
			# TODO intensity
			@scene.add(@scene_amb)
		end
		
		# Load .obj models and set up spotlights
		if json["objects"]
			@scene_spotlights = Hash.new
			json["objects"]["spotlights"].each do |e|
				name = e[0]
				data = e[1]
				
				@scene_spotlights[name] = Mittsu::SpotLight.new(data["color"].to_i(16), data["intensity"])
				@scene_spotlights[name].position.set(data["pos"][0], data["pos"][1], data["pos"][2])
				@scene_spotlights[name].cast_shadow = true
				@scene_spotlights[name].shadow_darkness = 0.5
				@scene_spotlights[name].shadow_camera_visible = DEBUG
				@scene.add(@scene_spotlights[name])
			end if json["objects"]["spotlights"]
			
	
			loader = Mittsu::OBJMTLLoader.new
			@scene_models = Hash.new
			json["objects"]["models"].each do |e|
				name = e[1]["file"]
				
				@scene_models[e[0]] = loader.load(File.expand_path("../3d/" + name + "/" + name + ".obj", __FILE__), name + ".mtl")
				
				if e[1]["pos"] then
					vec = e[1]["pos"]
					@scene_models[e[0]].position = Mittsu::Vector3.new(vec[0], vec[1], vec[2])
				end
				
				if e[1]["scale"] then
					vec = e[1]["scale"]
					@scene_models[e[0]].scale = Mittsu::Vector3.new(vec[0], vec[1], vec[2])
				end
				
				if e[1]["rot"] then
					vec = e[1]["rot"]
					@scene_models[e[0]].rotation.x = vec[0]
					@scene_models[e[0]].rotation.y = vec[1]
					@scene_models[e[0]].rotation.z = vec[2]
				end
				
				@scene_models[e[0]].receive_shadow = true
				@scene_models[e[0]].cast_shadow = true
				
				@scene_models[e[0]].traverse do |child|
					child.receive_shadow = true
					child.cast_shadow = true
				end
				
				@scene.add @scene_models[e[0]]
			end if json["objects"]["models"]
		end
		
		# Adds a veil that can be used to fade in/out the scene
		if json["veil"]
			@veil = Mittsu::Mesh.new(Mittsu::BoxGeometry.new(800, 600, 1),
				Mittsu::MeshLambertMaterial.new({color: json["veil"].to_i(16), transparent: true, opacity: 0}))
			@veil.position.x = -400
			@veil.position.y = 300
			@hud_scene.add @veil
		end
		
		# Loads and plays BGM
		if json["bgm"]
			@sound_bgm = Audio.new("audio/" + json["bgm"], true)
			@sound_bgm.play
		end
	end
	
	def update(r)
	end
	
	# Called when the mouse moves.
	# pos is a Mittsu::Vector2.
	def mouse(pos)
	end
	
	# Called when a key is typed.
	def key(k)
	end
	
	# Called when a key is released.
	def key_release(k)
	end
	
	# Should be overloaded to return the class that is to be loaded when chaning scene.
	def next_scene
	end
end
