# Timer.get returns a value that varies from v0 to vf according to time.
# Can be used for tweening.
class Timer
	def initialize(t0 = 0, Δt = 1, v0 = 0, vf = 0)
		@t0 = t0
		@Δt = Δt
		
		@v0 = v0
		@vf = vf
		@v = v0
	end
	
	def get
		@v
	end
	
	def done?
		if @v0 < @vf # Timer croissant
			@v >= @vf
		elsif @v0 > @vf # Timer décroissant
			@v <= @vf
		else # Timer nul
			true
		end
	end
	
	def update(t)
		if not self.done?
			Δ = t-@t0
			
			if @v0 < @vf # Timer croissant
				@v = @v0*(1-Δ/@Δt) + @vf*Δ/@Δt
			elsif @v0 > @vf # Timer décroissant
				@v = @v0*(1-Δ/@Δt) - @vf*Δ/@Δt
			end
		end
	end
end

# Manages multiples timers.
# Add timers to the .timers Hash.
# Use it to update all your Timer's at once.
class TimerBank
	attr_accessor :timers
	
	def initialize
		@timers = Hash.new
	end
	
	# is every Timer done?
	def done?
		done = true
		@timers.each do |k, tm|
			done = false if not tm.done?
		end
		done
	end
	
	def update(t)
		@timers.each do |k, tm|
			tm.update(t)
		end
	end
end

# Runs actions according to a defined series of actions.
# Example:
#
#   z = 0
#   tl = Timeline.new
#   tl.add(5, lambda {|t| puts "5!"})
#   tl.add(10, lambda {|t| z = z + t})
#   puts z
#   tl.update(1)
#   tl.update(6.66)
#   tl.update(20)
#   puts z
class Timeline
	def initialize(t0 = 0, continuous = false)
		@tl = Array.new
		
		@continuous = continuous
		@t0 = t0
	end
	
	def add(t, callback)
		@tl.push({:t => t, :callback => callback})
	end
	
	def done?
		@tl.empty?
	end
	
	def update(t)
		if not @tl.empty?
			if @tl.first[:t] < t - @t0
				@tl.first[:callback].call(t-@t0)
				@tl.shift
			end
			
			@tl.first[:callback].call(t-@t0) if @continuous and @tl.first
		end
	end
end
