class SceneIntro < Scene
	def initialize
		# Loads ressources from scene_intro.json
		super("scene_intro")
		
		@change_scene = false
	end
	
	def key(k)
		@change_scene = true
	end
	
	def update(r)
		# Gets the time elapsed since the loading of the scene.
		# @c is an instance of Mittsu::Clock
		t = 1000*@c.get_elapsed_time
		
		# Make the logo move
		@scene_overlays["logo"].position.x = -420 + 100*Math.sin(10*t)
	end
	
	def next_scene
		# Go to scene MyGame when a key is pressed
		MyGame if @change_scene
	end
end
