class MyGame < Scene
	def initialize
		super("my_game")
		
		@ball = Mittsu::Mesh.new(Mittsu::SphereGeometry.new(1), Mittsu::MeshLambertMaterial.new({color: 0x3cffbb}))
		@ball.position.z = -5
		@ball.position.y = 10
		@scene.add @ball
	end
	
	def key(k)
		# Basket controls
		if k == 263 # Left
			@scene_models["torus"].position.x -= 0.5
		elsif k == 262 # Right
			@scene_models["torus"].position.x += 0.5
		end
	end
	
	def update(r)
		t = 1000*@c.get_elapsed_time
		
		# Make the ball fall
		@ball.position.y -= 0.1
		
		if @ball.position.y < -5
			@ball.position.y = 10
			@ball.position.x = 10*rand-5
		end
	end
end
