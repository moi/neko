$LOAD_PATH.unshift File.expand_path("./lib", __dir__)
require "mittsu"

require "./config.rb"

require "./display.rb"
require "./sound.rb"
require "./time.rb"
require "./scene.rb"

Dir[File.dirname(__FILE__) + "/scenes/*.rb"].each {|file| require file }

renderer = Mittsu::OpenGLRenderer.new width: SCREEN_WIDTH, height: SCREEN_HEIGHT, title: "NekoEngine"
renderer.auto_clear = false

renderer.window.on_resize do |width, height|
	renderer.set_viewport(0, 0, height*ASPECT, height)
end

scene = SceneIntro.new

renderer.window.set_mouselock true

renderer.window.on_key_typed do |k| scene.key k end
renderer.window.on_key_released do |k| scene.key_release k end
renderer.window.on_mouse_move do |pos| scene.mouse pos end

renderer.window.run do
	ns = scene.next_scene

	if ns
		Audio.stop_all_audio
		puts "Scene change: " + ns.name if DEBUG
		scene = SceneLoad.new
		t = Thread.start { scene = ns.new }
	end

	scene.update renderer
	
	renderer.clear
	renderer.render scene.scene, scene.camera
	renderer.render scene.hud_scene, scene.hud_camera
	
	if DEBUG
		puts scene.camera.position if renderer.window.key_down?(GLFW_KEY_F2)
		puts scene.camera.rotation.x.to_s + "," + scene.camera.rotation.y.to_s + "," + scene.camera.rotation.z.to_s if renderer.window.key_down?(GLFW_KEY_F2)
		
		DisplayUtil.fps_controls(scene.camera, renderer) if renderer.window.key_down?(GLFW_KEY_F3)
		DEBUG_FASTLOAD = false if renderer.window.key_down?(GLFW_KEY_F7)
	end
end
