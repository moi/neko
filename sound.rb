require "sdl"

SDL::init(SDL::INIT_AUDIO)
SDL::Mixer.open

# Loads and plays sound files
class Audio
	def initialize(path, loop = false)
		@wave = SDL::Mixer::Wave.load path if not DEBUG_FASTLOAD
		@loop = loop
		
		set_vol 1
	end

	def play
		l = 1 if @loop
		l = 0 if not @loop
		SDL::Mixer.play_channel(0, @wave, l) if not DEBUG_FASTLOAD
	end
	
	# Stops the sound with an optional fade out
	def stop(fadeout_ms = 0)
		if fadeout_ms == 0
			SDL::Mixer.halt 0
		else
			SDL::Mixer.fade_out_music fadeout_ms
		end
	end
	
	def playing?
		SDL::Mixer.play? 0
	end
	
	# Sets volume, v = 0~1
	def set_vol(v)
		SDL::Mixer.set_volume(0, v*128)
	end
	
	def self.stop_all_audio
		SDL::Mixer.halt -1
	end
end


