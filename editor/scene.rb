class SceneWindow < Qt::MainWindow
	slots "selectItem()", "clickSave()", "clickRun()", "clickEditCode()", "clickAddModel()", "clickAddOverlay()"
	
	def initialize(path, name)
		super()
		
		@path, @name = path, name
		
		@ui = Ui_SceneUI.new
	    @ui.setupUi(self)
	    
	    @ui.treeWidget.header.close
	    
	    setWindowTitle("Scene Editor / " + name)
	    
	    file = @path + "/scenes/" + @name + ".json"
		@json = JSON.parse(File.read(file))
	    
	    populate
	end
	
	def populate
		setUpdatesEnabled false
		
		@ui.treeWidget.clear
		
		# https://doc.qt.io/qt-4.8/qtreewidgetitem.html#details
		
		i = Qt::TreeWidgetItem.new(@ui.treeWidget)
		i.setText 0, "Audio"
		@ui.treeWidget.addTopLevelItem(i)
		
		# Parse environment
		i = Qt::TreeWidgetItem.new(@ui.treeWidget)
		i.setText 0, "Environment"
		@ui.treeWidget.addTopLevelItem(i)
		
		j = Qt::TreeWidgetItem.new i
		j.setText 0, "Skybox"
		@ui.treeWidget.addTopLevelItem(i)
		
		j = Qt::TreeWidgetItem.new i
		j.setText 0, "Camera"
		@ui.treeWidget.addTopLevelItem(i)
		
		j = Qt::TreeWidgetItem.new i
		j.setText 0, "Ambient light"
		@ui.treeWidget.addTopLevelItem(i)
		
		# Parse models and overlays
		i = Qt::TreeWidgetItem.new(@ui.treeWidget)
		i.setText 0, "Models"
		@ui.treeWidget.addTopLevelItem(i)
		
		if @json["objects"] and @json["objects"]["models"]
			@json["objects"]["models"].each do |e|
				j = Qt::TreeWidgetItem.new i
				j.setText 0, e[0]
				@ui.treeWidget.addTopLevelItem(j)
			end
		end
		
		i = Qt::TreeWidgetItem.new(@ui.treeWidget)
		i.setText 0, "Overlays"
		@ui.treeWidget.addTopLevelItem(i)
		
		if @json["overlays"]
			@json["overlays"].each do |e|
				j = Qt::TreeWidgetItem.new i
				j.setText 0, e[0]
				@ui.treeWidget.addTopLevelItem(j)
			end
		end
		
		@ui.treeWidget.expandAll
		
		setUpdatesEnabled true
	end
	
	def set_edited(v)
		setWindowTitle("*Scene Editor / " + @name) if v
		setWindowTitle("Scene Editor / " + @name) if not v
	end
	
	def clickSave
		File.open(@path + "/scenes/" + @name + ".json", "w") do |f|
			f.write(JSON.pretty_generate(@json))
		end
		
		set_edited false
	end
	
	def selectItem
		i = @ui.treeWidget.selectedItems[0]
		#~ puts i.text(0)
		#~ puts i.parent.text(0) if i.parent
		
		#~ d = JSONEditor.new
		#~ d.show
	end
	
	def clickRun
		puts "TODO"
		
		Dir.chdir(@path) do
			system("ruby main.rb -scene " + @name) 
		end
	end
	
	def clickEditCode
		system("xdg-open " + @path + "/scenes/" + @name + ".json")
		system("xdg-open " + @path + "/scenes/" + @name + ".rb")
	end
	
	def clickAddOverlay
		f = Qt::FileDialog.getOpenFileName(self)
		n = Qt::InputDialog.getText(self, "Add overlay", "Name ?")
		
		if n and f and not n.empty?
			FileUtils.copy_file(f, @path + "/2d/" + n + ".png")
			
			@json["overlays"] = {} if not @json["overlays"]
			
			@json["overlays"][n] = {
				"file" => n + ".png",
				"size" => [64, 64],
				"pos" => [0, 0]}
			
			populate
			
			set_edited true
		end
	end
	
	def clickAddModel
		f = Qt::FileDialog.getOpenFileName(self)
		n = Qt::InputDialog.getText(self, "Add model", "Name ?")
		
		# TODO make directory, copy obj and mtl
		
		#~ @json["objects"]["models"][n] = { "file" => n }
		
		populate
	end
end
