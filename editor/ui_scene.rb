=begin
** Form generated from reading ui file 'ui_scene.ui'
**
** Created: Sun Apr 16 11:27:53 2017
**      by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
=end

class Ui_SceneUI
    attr_reader :actionEdit_code
    attr_reader :actionAdd_model
    attr_reader :actionAdd_overlay
    attr_reader :actionAdd_spotlight
    attr_reader :actionRun
    attr_reader :actionSave
    attr_reader :centralwidget
    attr_reader :gridLayout
    attr_reader :treeWidget
    attr_reader :menubar
    attr_reader :menuAdd
    attr_reader :menuScene

    def setupUi(sceneUI)
    if sceneUI.objectName.nil?
        sceneUI.objectName = "sceneUI"
    end
    sceneUI.resize(318, 431)
    @actionEdit_code = Qt::Action.new(sceneUI)
    @actionEdit_code.objectName = "actionEdit_code"
    @actionAdd_model = Qt::Action.new(sceneUI)
    @actionAdd_model.objectName = "actionAdd_model"
    @actionAdd_overlay = Qt::Action.new(sceneUI)
    @actionAdd_overlay.objectName = "actionAdd_overlay"
    @actionAdd_spotlight = Qt::Action.new(sceneUI)
    @actionAdd_spotlight.objectName = "actionAdd_spotlight"
    @actionRun = Qt::Action.new(sceneUI)
    @actionRun.objectName = "actionRun"
    @actionSave = Qt::Action.new(sceneUI)
    @actionSave.objectName = "actionSave"
    @centralwidget = Qt::Widget.new(sceneUI)
    @centralwidget.objectName = "centralwidget"
    @gridLayout = Qt::GridLayout.new(@centralwidget)
    @gridLayout.margin = 0
    @gridLayout.objectName = "gridLayout"
    @treeWidget = Qt::TreeWidget.new(@centralwidget)
    @treeWidget.objectName = "treeWidget"
    @sizePolicy = Qt::SizePolicy.new(Qt::SizePolicy::Expanding, Qt::SizePolicy::Expanding)
    @sizePolicy.setHorizontalStretch(0)
    @sizePolicy.setVerticalStretch(0)
    @sizePolicy.heightForWidth = @treeWidget.sizePolicy.hasHeightForWidth
    @treeWidget.sizePolicy = @sizePolicy
    @treeWidget.setProperty("showDropIndicator", Qt::Variant.new(false))

    @gridLayout.addWidget(@treeWidget, 0, 0, 1, 1)

    sceneUI.centralWidget = @centralwidget
    @menubar = Qt::MenuBar.new(sceneUI)
    @menubar.objectName = "menubar"
    @menubar.geometry = Qt::Rect.new(0, 0, 318, 19)
    @menuAdd = Qt::Menu.new(@menubar)
    @menuAdd.objectName = "menuAdd"
    @menuScene = Qt::Menu.new(@menubar)
    @menuScene.objectName = "menuScene"
    sceneUI.setMenuBar(@menubar)

    @menubar.addAction(@menuScene.menuAction())
    @menubar.addAction(@menuAdd.menuAction())
    @menuAdd.addAction(@actionAdd_model)
    @menuAdd.addAction(@actionAdd_overlay)
    @menuAdd.addAction(@actionAdd_spotlight)
    @menuScene.addAction(@actionRun)
    @menuScene.addAction(@actionEdit_code)
    @menuScene.addAction(@actionSave)

    retranslateUi(sceneUI)
    Qt::Object.connect(@treeWidget, SIGNAL('itemDoubleClicked(QTreeWidgetItem*,int)'), sceneUI, SLOT('selectItem()'))
    Qt::Object.connect(@actionAdd_overlay, SIGNAL('triggered()'), sceneUI, SLOT('clickAddOverlay()'))
    Qt::Object.connect(@actionAdd_model, SIGNAL('triggered()'), sceneUI, SLOT('clickAddModel()'))
    Qt::Object.connect(@actionEdit_code, SIGNAL('triggered()'), sceneUI, SLOT('clickEditCode()'))
    Qt::Object.connect(@actionSave, SIGNAL('triggered()'), sceneUI, SLOT('clickSave()'))

    Qt::MetaObject.connectSlotsByName(sceneUI)
    end # setupUi

    def setup_ui(sceneUI)
        setupUi(sceneUI)
    end

    def retranslateUi(sceneUI)
    sceneUI.windowTitle = Qt::Application.translate("SceneUI", "Scene Editor", nil, Qt::Application::UnicodeUTF8)
    @actionEdit_code.text = Qt::Application.translate("SceneUI", "Edit code", nil, Qt::Application::UnicodeUTF8)
    @actionAdd_model.text = Qt::Application.translate("SceneUI", "Add model", nil, Qt::Application::UnicodeUTF8)
    @actionAdd_overlay.text = Qt::Application.translate("SceneUI", "Add overlay", nil, Qt::Application::UnicodeUTF8)
    @actionAdd_spotlight.text = Qt::Application.translate("SceneUI", "Add spotlight", nil, Qt::Application::UnicodeUTF8)
    @actionRun.text = Qt::Application.translate("SceneUI", "Run scene", nil, Qt::Application::UnicodeUTF8)
    @actionSave.text = Qt::Application.translate("SceneUI", "Save", nil, Qt::Application::UnicodeUTF8)
    @treeWidget.headerItem.setText(0, Qt::Application.translate("SceneUI", "Name", nil, Qt::Application::UnicodeUTF8))
    @menuAdd.title = Qt::Application.translate("SceneUI", "&Add", nil, Qt::Application::UnicodeUTF8)
    @menuScene.title = Qt::Application.translate("SceneUI", "&Scene", nil, Qt::Application::UnicodeUTF8)
    end # retranslateUi

    def retranslate_ui(sceneUI)
        retranslateUi(sceneUI)
    end

end

module Ui
    class SceneUI < Ui_SceneUI
    end
end  # module Ui

