=begin
** Form generated from reading ui file 'ui_list.ui'
**
** Created: Sun Apr 16 11:27:53 2017
**      by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
=end

class Ui_ListUI
    attr_reader :actionRun
    attr_reader :actionCreate_new
    attr_reader :actionSet_as_default
    attr_reader :actionEdit_config
    attr_reader :actionOpen_folder
    attr_reader :centralwidget
    attr_reader :gridLayout
    attr_reader :listWidget
    attr_reader :menubar
    attr_reader :menuProject
    attr_reader :menuScenes
    attr_reader :menuAssets
    attr_reader :menuHelp

    def setupUi(listUI)
    if listUI.objectName.nil?
        listUI.objectName = "listUI"
    end
    listUI.resize(346, 329)
    @actionRun = Qt::Action.new(listUI)
    @actionRun.objectName = "actionRun"
    @actionCreate_new = Qt::Action.new(listUI)
    @actionCreate_new.objectName = "actionCreate_new"
    @actionSet_as_default = Qt::Action.new(listUI)
    @actionSet_as_default.objectName = "actionSet_as_default"
    @actionEdit_config = Qt::Action.new(listUI)
    @actionEdit_config.objectName = "actionEdit_config"
    @actionOpen_folder = Qt::Action.new(listUI)
    @actionOpen_folder.objectName = "actionOpen_folder"
    @centralwidget = Qt::Widget.new(listUI)
    @centralwidget.objectName = "centralwidget"
    @gridLayout = Qt::GridLayout.new(@centralwidget)
    @gridLayout.margin = 0
    @gridLayout.objectName = "gridLayout"
    @listWidget = Qt::ListWidget.new(@centralwidget)
    @listWidget.objectName = "listWidget"

    @gridLayout.addWidget(@listWidget, 0, 0, 1, 1)

    listUI.centralWidget = @centralwidget
    @menubar = Qt::MenuBar.new(listUI)
    @menubar.objectName = "menubar"
    @menubar.geometry = Qt::Rect.new(0, 0, 346, 19)
    @menuProject = Qt::Menu.new(@menubar)
    @menuProject.objectName = "menuProject"
    @menuScenes = Qt::Menu.new(@menubar)
    @menuScenes.objectName = "menuScenes"
    @menuAssets = Qt::Menu.new(@menubar)
    @menuAssets.objectName = "menuAssets"
    @menuHelp = Qt::Menu.new(@menubar)
    @menuHelp.objectName = "menuHelp"
    listUI.setMenuBar(@menubar)

    @menubar.addAction(@menuProject.menuAction())
    @menubar.addAction(@menuScenes.menuAction())
    @menubar.addAction(@menuAssets.menuAction())
    @menubar.addAction(@menuHelp.menuAction())
    @menuProject.addAction(@actionRun)
    @menuProject.addAction(@actionEdit_config)
    @menuProject.addAction(@actionOpen_folder)
    @menuScenes.addAction(@actionCreate_new)
    @menuScenes.addAction(@actionSet_as_default)

    retranslateUi(listUI)
    Qt::Object.connect(@actionRun, SIGNAL('triggered()'), listUI, SLOT('clickRun()'))
    Qt::Object.connect(@actionEdit_config, SIGNAL('triggered()'), listUI, SLOT('clickEdit()'))
    Qt::Object.connect(@actionSet_as_default, SIGNAL('triggered()'), listUI, SLOT('clickSetDefault()'))
    Qt::Object.connect(@actionCreate_new, SIGNAL('triggered()'), listUI, SLOT('clickCreateScene()'))
    Qt::Object.connect(@actionOpen_folder, SIGNAL('triggered()'), listUI, SLOT('clickOpen()'))
    Qt::Object.connect(@listWidget, SIGNAL('itemClicked(QListWidgetItem*)'), listUI, SLOT('clickList()'))

    Qt::MetaObject.connectSlotsByName(listUI)
    end # setupUi

    def setup_ui(listUI)
        setupUi(listUI)
    end

    def retranslateUi(listUI)
    listUI.windowTitle = Qt::Application.translate("ListUI", "NekoEngine Editor", nil, Qt::Application::UnicodeUTF8)
    @actionRun.text = Qt::Application.translate("ListUI", "Run", nil, Qt::Application::UnicodeUTF8)
    @actionCreate_new.text = Qt::Application.translate("ListUI", "Create new", nil, Qt::Application::UnicodeUTF8)
    @actionSet_as_default.text = Qt::Application.translate("ListUI", "Set as default", nil, Qt::Application::UnicodeUTF8)
    @actionEdit_config.text = Qt::Application.translate("ListUI", "Edit config", nil, Qt::Application::UnicodeUTF8)
    @actionOpen_folder.text = Qt::Application.translate("ListUI", "Open folder", nil, Qt::Application::UnicodeUTF8)
    @menuProject.title = Qt::Application.translate("ListUI", "&Project", nil, Qt::Application::UnicodeUTF8)
    @menuScenes.title = Qt::Application.translate("ListUI", "&Scenes", nil, Qt::Application::UnicodeUTF8)
    @menuAssets.title = Qt::Application.translate("ListUI", "Assets", nil, Qt::Application::UnicodeUTF8)
    @menuHelp.title = Qt::Application.translate("ListUI", "Help", nil, Qt::Application::UnicodeUTF8)
    end # retranslateUi

    def retranslate_ui(listUI)
        retranslateUi(listUI)
    end

end

module Ui
    class ListUI < Ui_ListUI
    end
end  # module Ui

