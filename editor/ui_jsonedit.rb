=begin
** Form generated from reading ui file 'ui_jsonedit.ui'
**
** Created: Sun Apr 16 11:27:53 2017
**      by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
=end

class Ui_JSONEdit
    attr_reader :gridLayout
    attr_reader :plainTextEdit
    attr_reader :buttonBox

    def setupUi(jSONEdit)
    if jSONEdit.objectName.nil?
        jSONEdit.objectName = "jSONEdit"
    end
    jSONEdit.resize(400, 300)
    @gridLayout = Qt::GridLayout.new(jSONEdit)
    @gridLayout.objectName = "gridLayout"
    @plainTextEdit = Qt::PlainTextEdit.new(jSONEdit)
    @plainTextEdit.objectName = "plainTextEdit"

    @gridLayout.addWidget(@plainTextEdit, 0, 0, 1, 1)

    @buttonBox = Qt::DialogButtonBox.new(jSONEdit)
    @buttonBox.objectName = "buttonBox"
    @buttonBox.orientation = Qt::Horizontal
    @buttonBox.standardButtons = Qt::DialogButtonBox::Cancel|Qt::DialogButtonBox::Ok

    @gridLayout.addWidget(@buttonBox, 1, 0, 1, 1)


    retranslateUi(jSONEdit)
    Qt::Object.connect(@buttonBox, SIGNAL('accepted()'), jSONEdit, SLOT('accept()'))
    Qt::Object.connect(@buttonBox, SIGNAL('rejected()'), jSONEdit, SLOT('reject()'))

    Qt::MetaObject.connectSlotsByName(jSONEdit)
    end # setupUi

    def setup_ui(jSONEdit)
        setupUi(jSONEdit)
    end

    def retranslateUi(jSONEdit)
    jSONEdit.windowTitle = Qt::Application.translate("JSONEdit", "JSON Editor", nil, Qt::Application::UnicodeUTF8)
    end # retranslateUi

    def retranslate_ui(jSONEdit)
        retranslateUi(jSONEdit)
    end

end

module Ui
    class JSONEdit < Ui_JSONEdit
    end
end  # module Ui

