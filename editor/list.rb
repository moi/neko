class SceneList < Qt::MainWindow
	slots "clickList()", "clickRun()", "clickEdit()", "clickSetDefault()", "clickOpen()", "clickCreateScene()"
	
	def initialize(path)
		super()
		
		@path = path
		
		@ui = Ui_ListUI.new
	    @ui.setupUi(self)
	    
		populate
	end
	
	def populate
		@ui.listWidget.clear
		
	    Dir.glob(@path + "/scenes/*.rb").each do |f|
			s = f.split("/")[-1].split(".")
			s = s.first(s.size-1).join(".")
			@ui.listWidget.addItem(Qt::ListWidgetItem.new(s))
	    end
	end
	
	def clickList
		w = SceneWindow.new(@path, @ui.listWidget.currentItem.text)
		w.show
	end
	
	def clickRun
		Dir.chdir(@path) do
			system("ruby main.rb") 
		end
	end
	
	def clickEdit
		system("xdg-open " + @path + "/config.rb")
	end
	
	def clickOpen
		system("xdg-open " + @path)
	end
	
	def clickSetDefault
		puts("TODO")
	end
	
	def clickCreateScene
		n = Qt::InputDialog.getText(self, "Create scene", "Name ?")
		
		if n.class != NilClass and not n.empty? and not File.exists?(@path + "/scenes/" + n + ".rb")
			# Copy files from templates/
			FileUtils.copy_file("templates/empty_scene.json", @path + "/scenes/" + n + ".json")
			
			data = File.read("templates/empty_scene.rb") 
			data = data.gsub("empty_scene", n) 
			data = data.gsub("EmptyScene", n.split('_').collect(&:capitalize).join) 
			File.open(@path + "/scenes/" + n + ".rb", "w") do |f|
			  f.write(data)
			end
			
			populate
		end
	end
end
