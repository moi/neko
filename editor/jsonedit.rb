class JSONEditor < Qt::Dialog
	slots "accepted()", "rejected()"
	
	def initialize
		super
		
		@ui = Ui_JSONEdit.new
	    @ui.setupUi(self)
	end
	
	def configure(data)
	end
	
	def accepted
	end
	
	def rejected
	end
end
