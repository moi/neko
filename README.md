![](misc/logo.png)

# NekoEngine

NekoEngine is a 3D game engine and framework well suited for building video games, art installations and experiments.

  * Written in Ruby
  * Includes a scene editor
  * Uses the [Mittsu](https://github.com/jellymann/mittsu) 3D graphics library
  * Works on Linux and Windows

## Screenshots

![](misc/editor1.png)

The editor in action

![](misc/game1.png)

Examples of what NekoEngine can do

## Installing

NekoEngine requires mittsu and, GLFW and sdl for audio:

    sudo apt-get install libglfw3 # For Ubuntu
	gem install mittsu rubysdl

(See https://github.com/jellymann/mittsu#installation for more information on installing mittsu)

To get NekoEngine, clone the repo:

    https://framagit.org/moi/neko

You can then run the included demo game:

    ruby main.rb

# The editor

The editor is in a very early state and may not work as expected.

To use it, install Qt for Ruby (ruby-qt4 on Ubuntu) and then run editor/main.rb.

    sudo apt install ruby-qt4
    cd editor
    ruby main.rb

## Tutorial: Making a simple game

### Creating a scene

NekoEngine already provides two scenes, as defined in main.rb:

  * scene_intro: Will be loaded at startup. We will use this scene as a menu.
  * scene_load: Will be shown while changing scenes.

Using the editor, create a new scene named "my_game". This will create the following files:

  * my_game.rb: the source code for our scene
  * my_game.json: a description file that defines the ressources used by the scene

### Creating a menu in scene_intro

In NekoEngine, 2D objets are referred as "overlays".
Using the editor, import an image in "scene_intro" to use as a logo.

Next, in scene_intro.json, define the size and position of the logo.

Please note:

  * All images are required to be in PNG format and must be power-of-two sized.
  * 2D coordinates are defined from the bottom-left corner of the window.

The SceneIntro class, defined in [scene_intro.rb](scenes/scene_intro.rb) contains the code for the menu.

The result:

![](misc/tutor1.png)

### Creating the game: adding ressources

For this game, we will have the following objects:

  * A 3D model: 3d/torus/torus.obj
  * Music: audio/technogeek.ogg
  * A skybox: skybox/meadow/[...].png

In addition, we will create a sphere (a ball) at runtime.

It is not yet possible to add music, skyboxes or 3D models using the editor.
To do so, you will have to manually edit [my_game.json](scenes/my_game.json). Refer to the provided example for more information.

Please note:

  * All textures are required to be in PNG format and must be power-of-two sized.
  * 3D models needs to be in .obj format and tesselated

### Programming the game

See [my_game.rb](scenes/my_game.rb) for the complete code.

![](misc/tutor2.png)
