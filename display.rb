# Utility methods for the display
module DisplayUtil
	# Return the next biggest power of 2
	# https://stackoverflow.com/questions/466204/rounding-up-to-nearest-power-of-2
	def DisplayUtil.next_po2(v)
		v -= 1
	    v |= v >> 1
	    v |= v >> 2
	    v |= v >> 4
	    v |= v >> 8
	    v |= v >> 16
	    v += 1
	    v
	end
	
	# Makes a Mesh out of an image file
	def DisplayUtil.make_overlay_mesh(path, w, h)
		tex = Mittsu::ImageUtils.load_texture(File.join File.dirname(__FILE__), path)
		tex.offset = Mittsu::Vector2.new(0, 1-h/next_po2(h.to_i))
		tex.repeat = Mittsu::Vector2.new(w/next_po2(w.to_i), h/next_po2(h.to_i))
			
		Mittsu::Mesh.new(
			Mittsu::BoxGeometry.new(w, h, 1.0),
			Mittsu::MeshBasicMaterial.new(map: tex, transparent: true))
	end
	
	def DisplayUtil.θ(ry)
		if ry > 2*Math::PI
			θ(ry - 2*Math::PI)
		elsif ry >= 0
			ry
		else
			θ(2*Math::PI + ry)
		end
	end
	
	def DisplayUtil.fps_controls(cam, r, dx = 5, origin = nil, dmax = 0)
		pos = r.window.mouse_position
		cam.rotation.y = pos.x/SCREEN_HEIGHT
		
		up = Mittsu::Vector3.new(0, 1, 0)
		
		v = cam.get_world_direction.multiply_scalar(dx)
		v.apply_axis_angle(up, Math::PI/2) if r.window.key_down?(KEY_LEFT)
		v.apply_axis_angle(up, -Math::PI/2) if r.window.key_down?(KEY_RIGHT)
		v.apply_axis_angle(up, Math::PI) if r.window.key_down?(KEY_DOWN)
	
		next_position = cam.position.clone
		next_position.add v
	
		distance = next_position.distance_to origin if origin
		
		if r.window.key_down?(KEY_UP) or r.window.key_down?(KEY_DOWN) or r.window.key_down?(KEY_LEFT) or r.window.key_down?(KEY_RIGHT)
			cam.position = next_position if origin and distance < dmax
			cam.position = next_position if not origin
		end
	end
	
	def DisplayUtil.pointer_ray(cam, r)
		pos = r.window.mouse_position
		v = Mittsu::Vector3.new( (pos.x/SCREEN_WIDTH)*2-1, (pos.y/SCREEN_HEIGHT)*2+1, 0)
		v.unproject(cam)
		
		Mittsu::Raycaster.new(cam.position, v.sub(cam.position).normalize)
	end
end
